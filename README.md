Git clone and install packages with `yarn`.

Populate database:

```
# show command line parameters 
node bin/populate-vikunja.js

# only 100 lists - that works fine
node bin/populate-vikunja.js person-as-list -s /tmp/GoLand/___go_build_code_vikunja_io_api --delete-db

# generate 10.000 lists - this is problem to handle by UI
node bin/populate-vikunja.js person-as-list -s /tmp/GoLand/___go_build_code_vikunja_io_api --delete-db -c 10000
```
