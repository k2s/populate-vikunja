export async function keypress () {
  process.stdin.setRawMode(true)
  return new Promise(resolve => process.stdin.once('data', () => {
    process.stdin.setRawMode(false)
    resolve()
  }))
}

export function between (min, max) {
  return Math.floor(
    Math.random() * (max - min + 1) + min
  )
}
